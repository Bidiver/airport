import { Modal } from 'bootstrap';

// Интерфейс: Рейс
interface IFlight {
   // Заполнить!
}

// Интерфейс: Самолёт
interface IAirplane {
    id: number
    name: string
    cap: number
}

// Интерфейс: Аэропорт
interface IAirport {
    id: number
    name: string
}

const flights:   IFlight[]   = [];  // рейсы
const airplanes: IAirplane[] = [];  // самолёты
const airports:  IAirport[]  = [];  // аэропорты


const btnShowModal_flight   = document.getElementById("button_showModal_flight");
const btnShowModal_airplane = document.getElementById("button_showModal_airplane");
const btnShowModal_airport  = document.getElementById("button_showModal_airport");

const btnSave_airplane = document.getElementById("button_airplane_save");

// fetch("/data", {
//     method: "GET"
// }).then(r => {
//     if (r.status >= 200 && r.status < 300) {
//         return r.json()
//     } else {
//         throw new Error(r.statusText)
//     }
// }).then((data: any[]) => {
//     console.log(data)
// })

btnShowModal_flight.addEventListener("click", () => {
    const el = document.getElementById("modal_flight") as HTMLElement;
    const modal = new Modal(el);
    modal.show();
});

btnShowModal_airplane.addEventListener("click", () => {
    const el = document.getElementById("modal_airplane") as HTMLElement;
    const modal = new Modal(el);
    modal.show();
});

btnShowModal_airport.addEventListener("click", () => {
    const el = document.getElementById("modal_airport") as HTMLElement;
    const modal = new Modal(el);
    modal.show();
});


/** Обновление списка самолётов на форме рейса */
function updateSelect_flight_airplanes() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < airplanes.length; i++) {
        const option = document.createElement("option") as HTMLOptionElement;
        option.value = `${ i + 1 }`;
        option.innerHTML = airplanes[i].name;
        df.appendChild(option);
    }

    const select = document.getElementById("select_flight_airplane");
    select.innerHTML = "";
    select.appendChild(df);
}

/** Действия кнопок "Редактировать" и "Удалить" */
function rowAction(actionName: string, tableName: string, id: number) {
    return (ev: Event) => {
        switch (tableName) {
            case "flights":
                console.log(`${ tableName }: ${ actionName } id = ${ id }`);
                //
                // код отправки запроса на сервер
                //
                break;
            case "airplanes":
                console.log(`${ tableName }: ${ actionName } id = ${ id }`);
                //
                // код отправки запроса на сервер
                //
                break;
            case "airports":
                console.log(`${ tableName }: ${ actionName } id = ${ id }`);
                //
                // код отправки запроса на сервер
                //
                break;
        }
    }
}

// Создание кнопок действий ("Редактировать" и "Удалить") для строк
function createActionButtons(tableName: string, id: number): DocumentFragment {
    const df = document.createDocumentFragment();
    const btnEdit = document.createElement("button");
    btnEdit.className = "btn btn-warning btn-sm me-2";
    btnEdit.innerHTML = /*html*/`<img src="img/pencil.svg" width="16" height="16" class="d-inline-block align-text-top">`;
    btnEdit.addEventListener("click", rowAction("edit", tableName, id));

    const btnDelete = document.createElement("button");
    btnDelete.className = "btn btn-danger btn-sm";
    btnDelete.innerHTML = /*html*/`<img src="img/trash.svg" width="16" height="16" class="d-inline-block align-text-top">`;
    btnEdit.addEventListener("click", rowAction("edit", tableName, id));

    df.appendChild(btnEdit);
    df.appendChild(btnDelete);
    return df
}

/** Обновление таблицы "Самолёты" */
function updateTable_airplane() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < airplanes.length; i++) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");
        td.appendChild(createActionButtons("airplanes", i + 1));
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ i + 1 }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = airplanes[i].name;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ airplanes[i].cap }`;
        tr.appendChild(td);

        df.appendChild(tr);

        // content += /*html*/`
        // <tr>
        //     <td>
        //         <button class="btn btn-warning btn-sm">E</button>
        //         <button class="btn btn-danger btn-sm">X</button>
        //     </td>
        //     <td>${ i + 1 }</td>
        //     <td>${ airplanes[i].name }</td>
        //     <td>${ airplanes[i].cap  }</td>
        // </tr>
        // `
    }

    const tbody = document.getElementById("tbody_airplanes");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}



/** Сохранение */

btnSave_airplane.addEventListener("click", () => {
    const name = (document.getElementById("input_airplane_name") as HTMLInputElement).value;
    const cap  = Number((document.getElementById("input_airplane_cap")  as HTMLInputElement).value);

    airplanes.push({
        id: 0,
        name,
        cap
    });

    updateTable_airplane();
    updateSelect_flight_airplanes();
});