package main

import (
	"Airport/Config"
	"Airport/Models"
	"Airport/Routes"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {

	var err error

	// Инициализация БД
	conn := "host=localhost user=postgres password=postgres dbname=Airport sslmode=disable"
	Config.DB, err = gorm.Open(postgres.Open(conn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	// Миграция БД
	Config.DB.AutoMigrate(
		&Models.Airport{},
		&Models.Airplane{},
		&Models.Flight{})

	// Инициализация веб-сервера
	r := gin.Default()
	r.Static("/public", "../client/public")

	r.LoadHTMLFiles("../client/public/main.html")

	r.GET("/", Routes.Root)

	r.GET("/data", Routes.GetData)

	r.GET("/airports", Routes.GetAirports)    // получение всех аэропортов
	r.GET("/airports/:id", Routes.GetAirport) // получение аэропорта по id

	r.GET("/airplanes", Routes.GetAirplanes)    // получение всех самолетов
	r.GET("/airplanes/:id", Routes.GetAirplane) // получение самолета по id

	r.GET("/flights", Routes.GetFlights)    // получение всех рейсов
	r.GET("/flights/:id", Routes.GetFlight) // получение рейса по id

	// Запуск веб-сервера
	r.Run(":7000")
}
