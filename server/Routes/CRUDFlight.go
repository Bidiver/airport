package Routes

import (
	"Airport/Config"
	"Airport/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GET /airplanes
// Получаем список всех самолетов
func GetAirplanes(context *gin.Context) {
	var airplanes []Models.Airplane
	Config.DB.Find(&airplanes)

	context.JSON(http.StatusOK, gin.H{"airplanes": airplanes})
}

// GET /airplanes/:id
// Получение одного самолета по ID
func GetAirplane(context *gin.Context) {
	// Проверяем имеется ли запись
	var airplane Models.Airplane
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&airplane).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"airplane": airplane})
}
