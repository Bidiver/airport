package Routes

import (
	"Airport/Config"
	"Airport/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GET /airplanes
// Получаем список всех рейсов
func GetFlights(context *gin.Context) {
	var flights []Models.Flight
	Config.DB.Find(&flights)

	context.JSON(http.StatusOK, gin.H{"flights": flights})
}

// GET /airplanes/:id
// Получение одного рейса по ID
func GetFlight(context *gin.Context) {
	// Проверяем имеется ли запись
	var flight Models.Flight
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&flight).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"flight": flight})

}
