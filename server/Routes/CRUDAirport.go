package Routes

import (
	"Airport/Config"
	"Airport/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GET /airports
// Получение всех аэропортов
func GetAirports(context *gin.Context) {
	var airports []Models.Airport
	Config.DB.Find(&airports)

	context.JSON(http.StatusOK, gin.H{"airoports": airports})
}

// GET /airplanes/:id
// Получение одного аэропорта по ID
func GetAirport(context *gin.Context) {
	// Проверяем имеется ли запись
	var airport Models.Airport
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&airport).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"airoport": airport})
}
