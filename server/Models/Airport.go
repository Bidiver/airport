package Models

// Аэропорт
type Airport struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название
}
