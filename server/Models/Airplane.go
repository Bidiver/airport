package Models

// Самолёт
type Airplane struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название
	Cap  int    `json:"cap"`  // Число мест
}
